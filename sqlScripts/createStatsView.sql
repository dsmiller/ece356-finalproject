-- Create the Statistics Tables and corresponding view

tee outfile.txt

drop table if exists LsoaStatsByMonth;

select '---------------------------------------------------------------------------------------' as '';
select 'Create LsoaStatsByMonth Table' as '';

CREATE TABLE LsoaStatsByMonth (
    LSOA_ID decimal(8),
    year int,
    month int,
    TotalCrimes int,
    TotalViolentCrimes int,
    foreign key (LSOA_ID) references LSOA(LSOA_ID),
    primary key (LSOA_ID, year, month)
);

select '---------------------------------------------------------------------------------------' as '';
select 'Fill LsoaStatsByMonth Table' as '';

INSERT INTO LsoaStatsByMonth
SELECT LSOA_ID, year, month, COUNT(*) AS TotalCrimes, SUM(violent) as TotalViolentCrimes
    FROM Crimes
    INNER JOIN LSOA USING (LSOA_ID)
    INNER JOIN CrimeTypes USING (crimeType_ID)
GROUP BY LSOA_ID, year, month;

select '---------------------------------------------------------------------------------------' as '';
select 'Create Procedure for LsoaStatsByMonth Table' as '';

DROP PROCEDURE IF EXISTS refresh_LsoaStatsByMonth;

DELIMITER $$
CREATE PROCEDURE refresh_LsoaStatsByMonth()
BEGIN
    TRUNCATE TABLE LsoaStatsByMonth;

	INSERT INTO LsoaStatsByMonth
	SELECT LSOA_ID, year, month, COUNT(*) AS TotalCrimes, SUM(violent) as TotalViolentCrimes
	    FROM Crimes
	    INNER JOIN LSOA USING (LSOA_ID)
	    INNER JOIN CrimeTypes USING (crimeType_ID)
	GROUP BY LSOA_ID, year, month;
END$$
DELIMITER ;

select '---------------------------------------------------------------------------------------' as '';
select 'Create Event to call procedure for LsoaStatsByMonth Table' as '';

CREATE EVENT update_LsoaStatsByMonth
    ON SCHEDULE EVERY 1 MONTH -- Once a month since we group by month
    DO CALL refresh_LsoaStatsByMonth();

select '---------------------------------------------------------------------------------------' as '';
select 'Create LsoaStats View' as '';

drop view if exists LsoaStats;

CREATE VIEW LsoaStats AS
WITH LsoaData AS (
	SELECT LSOA_ID, TotalCrimes, TotalViolentCrimes, (12 * (year - 2014) + month) as dateValue
	FROM LsoaStatsByMonth
), RegressionStats AS (
	SELECT LSOA_ID, 
	    SUM(TotalCrimes) as TotalCrimes,
	    SUM(TotalViolentCrimes) * 100.0 / sum(TotalCrimes) AS PercentViolentCrimes,
	    SUM(dateValue) AS Sdate,
	    SUM(dateValue * dateValue) AS SdateSq,
	    SUM(TotalCrimes * dateValue) AS SdateCrimes,
	    Count(*) AS NumMonths
	FROM LsoaData
	GROUP BY LSOA_ID
)
SELECT LSOA_ID, TotalCrimes, PercentViolentCrimes, 
	   ((NumMonths * SdateCrimes) - (TotalCrimes * Sdate)) 
	   / ((NumMonths * SdateSq) - (Sdate * Sdate)) AS crimeRateSlope
FROM RegressionStats;

select '---------------------------------------------------------------------------------------' as '';
select 'Create BoroughStatsByMonth View' as '';

drop view if exists BoroughStatsByMonth;

CREATE VIEW BoroughStatsByMonth AS
SELECT Borough_ID, 
    year,
    month,
    SUM(TotalCrimes) AS TotalCrimes,
    SUM(TotalViolentCrimes) AS TotalViolentCrimes
    FROM LsoaStatsByMonth
    INNER JOIN LSOA USING (LSOA_ID)
GROUP BY Borough_ID, year, month;

select '---------------------------------------------------------------------------------------' as '';
select 'Create BoroughStats View' as '';

drop view if exists BoroughStats;

CREATE VIEW BoroughStats AS
WITH BoroughData AS (
	SELECT Borough_ID, TotalCrimes, TotalViolentCrimes, (12 * (year - 2014) + month) as dateValue
	FROM BoroughStatsByMonth
), RegressionStats AS (
	SELECT Borough_ID, 
	    SUM(TotalCrimes) as TotalCrimes,
	    SUM(TotalViolentCrimes) * 100.0 / sum(TotalCrimes) AS PercentViolentCrimes,
	    SUM(dateValue) AS Sdate,
	    SUM(dateValue * dateValue) AS SdateSq,
	    SUM(TotalCrimes * dateValue) AS SdateCrimes,
	    Count(*) AS NumMonths
	FROM BoroughData
	GROUP BY Borough_ID
)
SELECT Borough_ID, TotalCrimes, PercentViolentCrimes, 
	   ((NumMonths * SdateCrimes) - (TotalCrimes * Sdate)) 
	   / ((NumMonths * SdateSq) - (Sdate * Sdate)) AS crimeRateSlope
FROM RegressionStats;