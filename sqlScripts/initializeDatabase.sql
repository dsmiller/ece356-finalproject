-- Initialize tables

tee outfile.txt

select '---------------------------------------------------------------------------------------' as '';
select 'drop duplicate tables' as '';

drop table if exists Crimes;
drop table if exists CrimeDetails;
drop table if exists Reports;
drop table if exists LSOA;
drop table if exists Boroughs;
drop table if exists CrimeTypes;

select '---------------------------------------------------------------------------------------' as '';
select 'Create LSOA Table' as '';

create table LSOA (
	LSOA_ID decimal(8) primary key,
    LSOA_NM varchar(35) not null unique,
    Borough_ID decimal(8)
);

select '---------------------------------------------------------------------------------------' as '';
select 'Load data into LSOA' as '';


INSERT IGNORE INTO LSOA(LSOA_ID, LSOA_NM, Borough_ID) 
SELECT 
	SUBSTRING(LSOA11CD, 2, 9),
	LSOA11NM,
	SUBSTRING(LAD16CD, 2, 9)
FROM LsoaToLad WHERE LSOA11CD IN 
	(select lsoa_code from LondonCrimeByLSOA) or LSOA11CD IN 
	(select lsoa_code from LondonStreet);

select '---------------------------------------------------------------------------------------' as '';
select 'Create Boroughs Table' as '';

create table Boroughs (
    Borough_ID decimal(8) primary key,
	Borough_NM varchar(32) not null unique
);

select '---------------------------------------------------------------------------------------' as '';
select 'Load data into Boroughs' as '';

INSERT IGNORE INTO Boroughs(Borough_ID, Borough_NM) SELECT distinct 
	SUBSTRING(LAD16CD, 2, 9),
	LAD16NM
FROM LsoaToLad WHERE SUBSTRING(LAD16CD, 2, 9) IN (select Borough_ID from LSOA);

select 'add FK on Boroughs' as '';
ALTER TABLE LSOA ADD FOREIGN KEY (Borough_ID) references Boroughs(Borough_ID);

select '---------------------------------------------------------------------------------------' as '';
select 'Create CrimeTypes Table' as '';

create table CrimeTypes (
	crimeType_ID int AUTO_INCREMENT primary key,
    crimeType_NM varchar (32) unique,
    violent boolean default false
);

select '---------------------------------------------------------------------------------------' as '';
select 'Load Data into CrimeTypes' as '';

INSERT IGNORE INTO CrimeTypes(crimeType_NM) SELECT 
	crime_type
FROM LondonStreet;

UPDATE CrimeTypes SET violent = true WHERE crimeType_NM = 'Robbery' OR crimeType_NM = 'Violence and sexual offences';

select '---------------------------------------------------------------------------------------' as '';
select 'Create Table Reports' as '';

create table Reports (
	report_ID int AUTO_INCREMENT primary key,
    year decimal(4),
    month decimal(2),
    LSOA_ID decimal(8),
    crimeType_ID int,
    longitude float,
    latitude float,
    location varchar(64),
    completed boolean default false,
    foreign key (LSOA_ID) references LSOA(LSOA_ID),
    foreign key (crimeType_ID) references CrimeTypes(crimeType_ID)
);


select '---------------------------------------------------------------------------------------' as '';
select 'drop duplicate tables' as '';

drop table if exists Crimes;
drop table if exists CrimeDetails;

select '---------------------------------------------------------------------------------------' as '';
select 'Create Table Crimes' as '';

create table Crimes (
    crime_ID int AUTO_INCREMENT primary key,
    year decimal(4),
    month decimal(2),
    LSOA_ID decimal(8),
    crimeType_ID int,
    foreign key (LSOA_ID) references LSOA(LSOA_ID),
    foreign key (crimeType_ID) references CrimeTypes(crimeType_ID)
);

select '---------------------------------------------------------------------------------------' as '';
select 'Create Table CrimeDetails' as '';

create table CrimeDetails (
    crime_ID int AUTO_INCREMENT primary key,
    reportedBy char(3),
    longitude float,
    latitude float,
    location varchar(64),
    outcome varchar(128),
    report_ID int,
    foreign key (report_ID) references Reports(report_ID)
);

select '---------------------------------------------------------------------------------------' as '';
select 'Load Data into Crimes from LondonStreet' as '';

INSERT INTO Crimes(year, month, LSOA_ID, crimeType_ID) SELECT 
    SUBSTRING(LondonStreet.month, 1, 4),
    SUBSTRING(LondonStreet.month, 6, 7),
    SUBSTRING(LondonStreet.lsoa_code, 2, 9),
    CrimeTypes.crimeType_ID
FROM LondonStreet Inner Join CrimeTypes on (LondonStreet.crime_type = CrimeTypes.crimeType_NM)
    Where not lsoa_code = '' and not longitude = 0 and not latitude = 0;

select '---------------------------------------------------------------------------------------' as '';
select 'Load Data into CrimeDetails from LondonStreet' as '';

select 'create temp reporter table for mapping' as '';
create table reporter (
    abbrev char(3),
    reportedBy varchar(64)
);

insert into reporter(abbrev, reportedBy) values ('MPS', 'Metropolitan Police Service');
insert into reporter(abbrev, reportedBy) values ('CLP', 'City of London Police');

select 'Load LondonStreet data' as '';

INSERT INTO CrimeDetails(reportedBy, longitude, latitude, location, outcome) SELECT 
    reporter.abbrev,
    LondonStreet.longitude,
    LondonStreet.latitude,
    LondonStreet.location,
    LondonStreet.outcome
FROM LondonStreet Inner Join reporter using (reportedBy)
    Where not lsoa_code = '' and not longitude = 0 and not latitude = 0;

select 'drop temp reporter table' as '';
Drop table reporter;

select '---------------------------------------------------------------------------------------' as '';
select 'Load Additional data into Crimes from LondonCrimeByLSOA' as '';

select 'create temp crimetype mapper table' as '';
drop table if exists CrimeMapper;
create table CrimeMapper (
    crimeType_ID int,
    minor_category varchar(64)
);

select 'manually populate crimetype mapper table' as '';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Other Sexual'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Rape'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Assault with Injury'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Common Assault'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Harassment'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Murder'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Other violence'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Wounding/GBH'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Burglary in a Dwelling'
From CrimeTypes where crimeType_NM = 'Burglary';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Burglary in Other Buildings'
From CrimeTypes where crimeType_NM = 'Burglary';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Criminal Damage To Dwelling'
From CrimeTypes where crimeType_NM = 'Criminal damage and arson';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Criminal Damage To Motor Vehicle'
From CrimeTypes where crimeType_NM = 'Criminal damage and arson';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Criminal Damage To Other Building'
From CrimeTypes where crimeType_NM = 'Criminal damage and arson';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Other Criminal Damage'
From CrimeTypes where crimeType_NM = 'Criminal damage and arson';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Drug Trafficking'
From CrimeTypes where crimeType_NM = 'Drugs';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Other Drugs'
From CrimeTypes where crimeType_NM = 'Drugs';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Possession Of Drugs'
From CrimeTypes where crimeType_NM = 'Drugs';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Business Property'
From CrimeTypes where crimeType_NM = 'Robbery';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Personal Property'
From CrimeTypes where crimeType_NM = 'Robbery';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Offensive Weapon'
From CrimeTypes where crimeType_NM = 'Possession of weapons';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Theft From Shops'
From CrimeTypes where crimeType_NM = 'Shoplifting';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Other Theft Person'
From CrimeTypes where crimeType_NM = 'Theft from the person';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Theft/Taking of Pedal Cycle'
From CrimeTypes where crimeType_NM = 'Bicycle theft';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Motor Vehicle Interference & Tampering'
From CrimeTypes where crimeType_NM = 'Vehicle crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Theft From Motor Vehicle'
From CrimeTypes where crimeType_NM = 'Vehicle crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Theft/Taking Of Motor Vehicle'
From CrimeTypes where crimeType_NM = 'Vehicle crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Counted per Victim'
From CrimeTypes where crimeType_NM = 'Other crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Other Fraud & Forgery'
From CrimeTypes where crimeType_NM = 'Other crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Going Equipped'
From CrimeTypes where crimeType_NM = 'Other crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Other Notifiable'
From CrimeTypes where crimeType_NM = 'Other crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Handling Stolen Goods'
From CrimeTypes where crimeType_NM = 'Other theft';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
    'Other Theft'
From CrimeTypes where crimeType_NM = 'Other theft';

Drop PROCEDURE if exists genEntriesFromSupplementaryTable;

DELIMITER $$
CREATE PROCEDURE genEntriesFromSupplementaryTable()
BEGIN
    DECLARE counter INT;
    DECLARE countMax INT;
    SET counter = 1;
    SET countMax = (select max(value) from LondonCrimeByLSOA);

    WHILE counter <= countMax DO
        select 'load data from values, counter is: ', counter as '';
        INSERT INTO Crimes(year, month, LSOA_ID, crimeType_ID) SELECT 
            LondonCrimeByLSOA.year,
            LondonCrimeByLSOA.month,
            SUBSTRING(LondonCrimeByLSOA.lsoa_code, 2, 9),
            CrimeMapper.crimeType_ID
            FROM LondonCrimeByLSOA Inner Join CrimeMapper using (minor_category)
                Where value >= counter and year >= 2014;
        SET counter  = counter  + 1;
    END WHILE;
END$$
DELIMITER ;

CALL genEntriesFromSupplementaryTable();

drop table CrimeMapper;

notee;