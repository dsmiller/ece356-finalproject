-- Import all raw data into tables
-- This is specifically for loading striaght from CSVs into the same format


tee outfile.txt

--Output_Area_to_Local_Authority_District_to_Lower_Layer_Super_Output_Area_to_Middle_Layer_Super_Output_Area_to_Local_Enterprise_Partnership_April_2017_Lookup_in_England_V2.csv
drop table if exists LsoaToLad;
--london_crime_by_lsoa.csv
drop table if exists LondonCrimeByLSOA;
--london-street.csv
drop table if exists LondonStreet;

select '---------------------------------------------------------------------------------------' as '';
select 'Create LsoaToLad Table' as '';

create table LsoaToLad (
    OA11CD char(10),
    LAD16CD char(10),
    LAD16NM varchar(64),
    LSOA11CD char(10),
    LSOA11NM varchar(64),
    PRIMARY KEY (OA11CD)
);

select '---------------------------------------------------------------------------------------' as '';
select 'Load London Area Mappers (LSOA to LAD)' as '';

load data infile '/var/lib/mysql-files/10-Crime/UKCrime/Output_Area_to_Local_Authority_District_to_Lower_Layer_Super_Output_Area_to_Middle_Layer_Super_Output_Area_to_Local_Enterprise_Partnership_April_2017_Lookup_in_England_V2.csv' ignore 
into table LsoaToLad
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n';

show warnings limit 10;

select '---------------------------------------------------------------------------------------' as '';
select 'Create LondonCrimeByLSOA Table' as '';

create table LondonCrimeByLSOA (
    lsoa_code char(10),
    borough varchar(64),
    major_category varchar(64),
    minor_category varchar(64),
    value int,
    year int,
    month int,
    PRIMARY KEY (lsoa_code, year, month, major_category, minor_category)
);

select '---------------------------------------------------------------------------------------' as '';
select 'Load LondonCrimeByLSOA' as '';

load data infile '/var/lib/mysql-files/10-Crime/UKCrime/london_crime_by_lsoa.csv' ignore 
into table LondonCrimeByLSOA
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 1 lines;

show warnings limit 10;

select '---------------------------------------------------------------------------------------' as '';
select 'Create LondonStreet Table' as '';

create table LondonStreet (
    crimeID varchar(64),
    month char(7),
    reportedBy varchar(64),
    fallsWithin varchar(64),
    longitude float,
    latitude float,
    location varchar(64),
    lsoa_code char(10),
    lsoa_name varchar(64),
    crime_type varchar(64),
    outcome varchar(128),
    context varchar(64)
);

select '---------------------------------------------------------------------------------------' as '';
select 'Load LondonStreet' as '';

load data infile '/var/lib/mysql-files/10-Crime/UKCrime/london-street.csv' ignore 
into table LondonStreet
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n'
     ignore 1 lines;

show warnings limit 10;
nowarning;
notee;