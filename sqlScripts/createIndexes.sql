-- Create an index with CrimeID to Date, LocationID, CrimeType
drop index if exists idx_CrimesTable;
CREATE INDEX idx_CrimesTable ON Crimes (crime_ID, year, month, LSOA_ID, crimeType_ID);

-- Create index for CrimeTypeID to isViolent
drop index if exists idx_CrimeTypeIsViolent;
CREATE INDEX idx_CrimeTypeIsViolent ON CrimeTypes (crimeType_ID, violent);

-- Create index for LSAO to Burrough
drop index if exists idx_LsoaBurrough;
CREATE INDEX idx_LsoaBurrough ON LSOA (LSOA_ID, Borough_ID);