

tee outfile.txt

select '---------------------------------------------------------------------------------------' as '';
select 'drop duplicate tables' as '';

drop table if exists Crimes;
drop table if exists CrimeDetails;

select '---------------------------------------------------------------------------------------' as '';
select 'Create Table Crimes' as '';

create table Crimes (
    crime_ID int AUTO_INCREMENT primary key,
    year decimal(4),
    month decimal(2),
    LSOA_ID decimal(8),
    crimeType_ID int,
    foreign key (LSOA_ID) references LSOA(LSOA_ID),
    foreign key (crimeType_ID) references CrimeTypes(crimeType_ID)
);

select '---------------------------------------------------------------------------------------' as '';
select 'Create Table CrimeDetails' as '';

create table CrimeDetails (
    crime_ID int AUTO_INCREMENT primary key,
    reportedBy char(3),
    longitude float,
    latitude float,
    location varchar(64),
    outcome varchar(128),
    report_ID int,
    foreign key (report_ID) references Reports(report_ID)
);

select '---------------------------------------------------------------------------------------' as '';
select 'Load Data into Crimes from LondonStreetTest' as '';

INSERT INTO Crimes(year, month, LSOA_ID, crimeType_ID) SELECT 
	SUBSTRING(LondonStreetTest.month, 1, 4),
	SUBSTRING(LondonStreetTest.month, 6, 7),
	SUBSTRING(LondonStreetTest.lsoa_code, 2, 9),
	CrimeTypes.crimeType_ID
FROM LondonStreetTest Inner Join CrimeTypes on (LondonStreetTest.crime_type = CrimeTypes.crimeType_NM)
	Where not lsoa_code = '' and not longitude = 0 and not latitude = 0;

select '---------------------------------------------------------------------------------------' as '';
select 'Load Data into CrimeDetails from LondonStreetTest' as '';

select 'create temp reporter table for mapping' as '';
create table reporter (
	abbrev char(3),
	reportedBy varchar(64)
);

insert into reporter(abbrev, reportedBy) values ('MPS', 'Metropolitan Police Service');
insert into reporter(abbrev, reportedBy) values ('CLP', 'City of London Police');

select 'Load LondonStreetTest data' as '';

INSERT INTO CrimeDetails(reportedBy, longitude, latitude, location, outcome) SELECT 
	reporter.abbrev,
	LondonStreetTest.longitude,
	LondonStreetTest.latitude,
	LondonStreetTest.location,
	LondonStreetTest.outcome
FROM LondonStreetTest Inner Join reporter using (reportedBy)
	Where not lsoa_code = '' and not longitude = 0 and not latitude = 0;

select 'drop temp reporter table' as '';
Drop table reporter;

select '---------------------------------------------------------------------------------------' as '';
select 'Load Additional data into Crimes from LondonCrimeByLSOATest' as '';

select 'create temp crimetype mapper table' as '';
drop table if exists CrimeMapper;
create table CrimeMapper (
	crimeType_ID int,
	minor_category varchar(64)
);

select 'manually populate crimetype mapper table' as '';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Other Sexual'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Rape'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Assault with Injury'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Common Assault'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Harassment'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Murder'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Other violence'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Wounding/GBH'
From CrimeTypes where crimeType_NM = 'Violence and sexual offences';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Burglary in a Dwelling'
From CrimeTypes where crimeType_NM = 'Burglary';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Burglary in Other Buildings'
From CrimeTypes where crimeType_NM = 'Burglary';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Criminal Damage To Dwelling'
From CrimeTypes where crimeType_NM = 'Criminal damage and arson';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Criminal Damage To Motor Vehicle'
From CrimeTypes where crimeType_NM = 'Criminal damage and arson';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Criminal Damage To Other Building'
From CrimeTypes where crimeType_NM = 'Criminal damage and arson';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Other Criminal Damage'
From CrimeTypes where crimeType_NM = 'Criminal damage and arson';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Drug Trafficking'
From CrimeTypes where crimeType_NM = 'Drugs';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Other Drugs'
From CrimeTypes where crimeType_NM = 'Drugs';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Possession Of Drugs'
From CrimeTypes where crimeType_NM = 'Drugs';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Business Property'
From CrimeTypes where crimeType_NM = 'Robbery';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Personal Property'
From CrimeTypes where crimeType_NM = 'Robbery';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Offensive Weapon'
From CrimeTypes where crimeType_NM = 'Possession of weapons';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Theft From Shops'
From CrimeTypes where crimeType_NM = 'Shoplifting';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Other Theft Person'
From CrimeTypes where crimeType_NM = 'Theft from the person';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Theft/Taking of Pedal Cycle'
From CrimeTypes where crimeType_NM = 'Bicycle theft';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Motor Vehicle Interference & Tampering'
From CrimeTypes where crimeType_NM = 'Vehicle crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Theft From Motor Vehicle'
From CrimeTypes where crimeType_NM = 'Vehicle crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Theft/Taking Of Motor Vehicle'
From CrimeTypes where crimeType_NM = 'Vehicle crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Counted per Victim'
From CrimeTypes where crimeType_NM = 'Other crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Other Fraud & Forgery'
From CrimeTypes where crimeType_NM = 'Other crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Going Equipped'
From CrimeTypes where crimeType_NM = 'Other crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Other Notifiable'
From CrimeTypes where crimeType_NM = 'Other crime';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Handling Stolen Goods'
From CrimeTypes where crimeType_NM = 'Other theft';
insert into CrimeMapper(crimeType_ID, minor_category) SELECT crimeType_ID,
	'Other Theft'
From CrimeTypes where crimeType_NM = 'Other theft';

Drop PROCEDURE if exists genEntriesFromSupplementaryTable;

DELIMITER $$
CREATE PROCEDURE genEntriesFromSupplementaryTable()
BEGIN
 	DECLARE counter INT;
	DECLARE countMax INT;
	SET counter = 1;
	SET countMax = (select max(value) from LondonCrimeByLSOATest);

  	WHILE counter <= countMax DO
    	select 'load data from values, counter is: ', counter as '';
    	INSERT INTO Crimes(year, month, LSOA_ID, crimeType_ID) SELECT 
			LondonCrimeByLSOATest.year,
			LondonCrimeByLSOATest.month,
			SUBSTRING(LondonCrimeByLSOATest.lsoa_code, 2, 9),
			CrimeMapper.crimeType_ID
			FROM LondonCrimeByLSOATest Inner Join CrimeMapper using (minor_category)
				Where value >= counter and year >= 2014;
	    SET counter  = counter  + 1;
 	END WHILE;
END$$
DELIMITER ;

CALL genEntriesFromSupplementaryTable();

drop table CrimeMapper;

notee;
