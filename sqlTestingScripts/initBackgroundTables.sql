-- Initialize tables

tee outfile.txt

select '---------------------------------------------------------------------------------------' as '';
select 'drop duplicate tables' as '';

drop table if exists Crimes;
drop table if exists CrimeDetails;
drop table if exists Reports;
drop table if exists LSOA;
drop table if exists Boroughs;
drop table if exists CrimeTypes;

select '---------------------------------------------------------------------------------------' as '';
select 'Create LSOA Table' as '';

create table LSOA (
	LSOA_ID decimal(8) primary key,
    LSOA_NM varchar(35) not null unique,
    Borough_ID decimal(8)
);

select '---------------------------------------------------------------------------------------' as '';
select 'Load data into LSOA' as '';


INSERT IGNORE INTO LSOA(LSOA_ID, LSOA_NM, Borough_ID) 
SELECT 
	SUBSTRING(LSOA11CD, 2, 9),
	LSOA11NM,
	SUBSTRING(LAD16CD, 2, 9)
FROM LsoaToLad WHERE LSOA11CD IN 
	(select lsoa_code from LondonCrimeByLSOATest) or LSOA11CD IN 
	(select lsoa_code from LondonStreetTest);


select '---------------------------------------------------------------------------------------' as '';
select 'Create Boroughs Table' as '';

create table Boroughs (
    Borough_ID decimal(8) primary key,
	Borough_NM varchar(32) not null unique
);

select '---------------------------------------------------------------------------------------' as '';
select 'Load data into Boroughs' as '';

INSERT IGNORE INTO Boroughs(Borough_ID, Borough_NM) SELECT distinct 
	SUBSTRING(LAD16CD, 2, 9),
	LAD16NM
FROM LsoaToLad WHERE SUBSTRING(LAD16CD, 2, 9) IN (select Borough_ID from LSOA);

select 'add FK on Boroughs' as '';
ALTER TABLE LSOA ADD FOREIGN KEY (Borough_ID) references Boroughs(Borough_ID);

select '---------------------------------------------------------------------------------------' as '';
select 'Create CrimeTypes Table' as '';

create table CrimeTypes (
	crimeType_ID int AUTO_INCREMENT primary key,
    crimeType_NM varchar (32) unique,
    violent boolean default false
);

select '---------------------------------------------------------------------------------------' as '';
select 'Load Data into CrimeTypes' as '';

INSERT IGNORE INTO CrimeTypes(crimeType_NM) SELECT 
	crime_type
FROM LondonStreetTest;

UPDATE CrimeTypes SET violent = true WHERE crimeType_NM = 'Robbery' OR crimeType_NM = 'Violence and sexual offences';

select '---------------------------------------------------------------------------------------' as '';
select 'Create Table Reports' as '';

create table Reports (
	report_ID int AUTO_INCREMENT primary key,
    year decimal(4),
    month decimal(2),
    LSOA_ID decimal(8),
    crimeType_ID int,
    longitude float,
    latitude float,
    location varchar(64),
    completed boolean default false,
    foreign key (LSOA_ID) references LSOA(LSOA_ID),
    foreign key (crimeType_ID) references CrimeTypes(crimeType_ID)
);

notee;


