
drop table if exists LondonStreetTest;
drop table if exists LondonCrimeByLSOATest;


Create table LondonStreetTest like LondonStreet;
Create table LondonCrimeByLSOATest like LondonCrimeByLSOA;

insert into LondonStreetTest Select * from LondonStreet limit 100000;
insert into LondonCrimeByLSOATest Select * from LondonCrimeByLSOA limit 100000;