select '---------------------------------------------------------------------------------------' as '';
select 'Create Procedure for LsoaStatsByMonth Table' as '';

DROP PROCEDURE IF EXISTS refresh_LsoaStatsByMonth;

DELIMITER $$
CREATE PROCEDURE refresh_LsoaStatsByMonth()
BEGIN
    TRUNCATE TABLE LsoaStatsByMonth;

	INSERT INTO LsoaStatsByMonth
	SELECT LSOA_ID, year, month, COUNT(*) AS TotalCrimes, SUM(violent) as TotalViolentCrimes
	    FROM Crimes
	    INNER JOIN LSOA USING (LSOA_ID)
	    INNER JOIN CrimeTypes USING (crimeType_ID)
	GROUP BY LSOA_ID, year, month;
END$$
DELIMITER ;

select '---------------------------------------------------------------------------------------' as '';
select 'Create Event to call procedure for LsoaStatsByMonth Table' as '';

CREATE EVENT update_LsoaStatsByMonth
    ON SCHEDULE EVERY 1 MONTH -- Once a month since we group by month
    DO CALL refresh_LsoaStatsByMonth();